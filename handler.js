'use strict';

const csv = require('csv-parser');
const Cloudant = require('@cloudant/cloudant');

const AWS = require('aws-sdk');
const s3 = new AWS.S3({
  accessKeyId: process.env.ACCESS_KEY_ID,
  secretAccessKey: process.env.SECRET_KEY_ID
});

/**
 * To connect cloudant database
 */
const dbConnect = () => {
  return new Promise((resolve, reject) => {
    Cloudant({
      host: process.env.CL_HOST,
      account: process.env.CL_ACCOUNT,
      password: process.env.CL_PASSWORD,
      requestDefaults: {
        timeout: 5000
      }
    }, ((err, cloudant) => {
      if(err) {
        console.error('Connect failure: ' + err.message + ' for Cloudant DB: ' +
        process.env.CL_DB);
        reject(err);
      } else {
        let db = cloudant.use(process.env.CL_DB);
        // console.log('Connect success! Connected to DB: ' + process.env.CL_DB);
        resolve(db);
      }
    }));
  });
};

/**
 * To actually perform import data to cloudant database
 */
const upsertAsync = (myDb, dataObj, iter = 2) => {
  return new Promise( async (resolve, reject) => {
    await myDb.get(dataObj["code"], async (err, foundData) => {
      if(Boolean(foundData) && Object.keys(foundData).length !== 0) {
        // Update
        dataObj["_rev"] = foundData["_rev"];
        dataObj["_id"] = foundData["_id"];
      } else {
        // Create
        dataObj["_id"] = 'random';
        dataObj["code"] = (dataObj["code"] || '').trim();
        if(dataObj["code"] != '') dataObj["_id"] = dataObj["code"];
      }
      await myDb.insert(dataObj)
      .then(data => {
        console.log(`Cloundant entry success ${data.id}`);
        resolve(data);
      }).catch(err => {
        if(iter > 0) {
          upsertAsync(myDb, dataObj, iter--).then(resolve).catch(reject);
        }
        else {
          reject(err);
        }
      });
    });
  });
};

/**
 * Single function to carry out import data operation
 */
module.exports.importData = async event => {

  await dbConnect().then( async (myDb) => {

    let startTime = new Date().getTime();

    const params = {
      Bucket: process.env.S3_BUCKET
    };

    await s3.listObjects(params, async (err, data) => {

      if(err) {
        console.error(err);
      }

      if(data.Contents != null && data.Contents.length > 0) {

        let finalResult = await data.Contents.map( async (item) => {

          if(item.Key.indexOf('file_part_') !== -1) {

            let tempData = [];

            let getParams = { Bucket: process.env.S3_BUCKET, Key: item.Key };
            let fileReadStream = s3.getObject(getParams).createReadStream();

            let processResult = await fileReadStream
            .pipe(csv({
              separator: ';',
              headers: [
                "code", "supplier_hotelcode", "name", "category", "address",
                "city", "tlc", "country_code", "geo (lat/lon)"
              ],
              mapValues: ({ header, index, value }) => value.replace(/\\n/g, ' ')
            }))
            .on('data', (dataObj) => {
              if(dataObj.code != "code") {
                return new Promise( async (resolve, reject) => {
                  let result = await upsertAsync(myDb, dataObj);
                  resolve(result);
                });
              }
            })
            .on('end', () => {
              console.log("done");
            });

            if(processResult != null) {

              let deleteParams = getParams;
              await s3.deleteObject(deleteParams, function (err, data) {
                if(err) {
                  console.log("Check if you have sufficient permissions : " + err);
                }
                if(data) {
                  console.log("Deleted successfull for file : "+ item.Key);
                }
              });
              return;

            }

          }

        });

      } else {
        console.log("No CSV files found.");
      }
    });

    let endTime = new Date().getTime();

    console.log("Time taken: ", endTime - startTime);

    return {
      statusCode: 200,
      body: JSON.stringify(
        {
          message: 'Function executed successfully!',
          input: event,
        },
        null,
        2
      ),
    };

  });

};

/**
 * Method to fetch files from s3 bucket and
 * invoke dynamic lambda to process import
 */
module.exports.fetchFiles = async event => {

  const lambda = new AWS.Lambda({
    region: "eu-west-1"
  });

  const params = {
    Bucket: process.env.S3_BUCKET
  };

  let files = [];

  s3.listObjects(params, async (err, data) => {

    if(err) {
      console.error(err);
    }

    if(data.Contents != null && data.Contents.length > 0) {

      files = data.Contents.reduce(( accumulator, item, index ) => {
        if(item.Key.indexOf('csv/file_part_') !== -1) {
          accumulator.push(item.Key);
        }
        return accumulator;
      }, []);

    }

    files.forEach( file => {

      const fileParams = {
        FunctionName: "anixeimport-anixe-process-import",
        InvocationType: "Event",
        Payload: JSON.stringify(file)
      };

      lambda.invoke(fileParams, (error, data) => {
        if(error) {
          console.error(JSON.stringify(error));
          return new Error(`Error printing messages: ${JSON.stringify(error)}`);
        } else if(data) {
          console.log(file);
        }
      });

    });

  });

};

/**
 * Method to process actual import
 */
module.exports.processImport = async event => {

  await dbConnect().then( async (myDb) => {
    let getParams = { Bucket: process.env.S3_BUCKET, Key: event };
    let fileReadStream = s3.getObject(getParams).createReadStream();
    let response = {};

    await fileReadStream
    .pipe(csv({
      separator: ';',
      headers: [
        "code", "supplier_hotelcode", "name", "category", "address",
        "city", "tlc", "country_code", "geo (lat/lon)"
      ],
      mapValues: ({ header, index, value }) => value.replace(/\\n/g, ' ')
    }))
    .on('data', (dataObj) => {
      if(dataObj.code != "code") {
        return new Promise( async (resolve, reject) => {
          let result = await upsertAsync(myDb, dataObj);
          resolve(result);
        });
      }
    })
    .on('close', async () => {
      let deleteParams = getParams;
      await s3.deleteObject(deleteParams, function (err, data) {
        if(err) {
          console.log("Check if you have sufficient permissions : " + err);
        }
        if(data) {
          console.log("Delete successful for file : "+ event);
          response = {
            statusCode: 200,
            body: JSON.stringify({
              message: `Import done for file: ${event}`
            })
          };
        }
      });
    });
    return response;
  }).catch((err) => {
    return err;
  });
};
