'use strict'

const path = require('path');
const fs = require('fs');

const ftpClient = require('ftp');
const ftpConfig = {
  host: process.env.FTP_HOST,
  connTimeout: process.env.FTP_CONNECTION_TIMEOUT
};

const c = new ftpClient();

/**
 * Method to download zip files using ftp
 */
module.exports.downloadFiles = async event => {

  let tempDownloadRes = [];

  return new Promise( async (resolve, reject) => {

    try {
      await c.connect(ftpConfig);
    } catch(err) {
      console.log("ftpConfig err", err);
    }

    await c.on('ready', async () => {
      await c.list( async (err, list) => {
        if(err) throw err;
        let tempList = list.filter(zipFiles);
        tempDownloadRes = await tempList.map(downloadFiles);
        c.end();
      });
    });

    resolve(true);

  });
}

const downloadFiles = async (file) => {
  return c.get(file.name, async (err, stream) => {
    if(err) throw err;
    try {
      stream.once('close', () => { c.end(); });
      let tempFilename = process.env.LOCAL_TEMP_PATH+file.name;
      await stream.pipe(fs.createWriteStream(tempFilename));
    } catch(err) {
      throw err;
    }
  });
};

const zipFiles = function(element) {
  return element.type !== 'd' && element.size <= 102400 && path.extname(element.name) === '.zip';
};

/**
 * Method to upload split files to destination
 */
module.exports.uploadFiles = async event => {

  const AWS = require('aws-sdk');
  const s3 = new AWS.S3({
    accessKeyId: process.env.ACCESS_KEY_ID,
    secretAccessKey: process.env.SECRET_KEY_ID
  });

  return new Promise( async (resolve, reject) => {
    const testFolder = process.env.LOCAL_SPLIT_PATH;
    let tempPromises = [];
    fs.readdirSync(testFolder).forEach(async file => {
      let tempFilename = testFolder+file;
      let tempCrs = await fs.createReadStream(tempFilename);
      if(file.indexOf(process.env.SPLIT_FILE_PREFIX) !== -1) {
        let params = {
          Bucket: process.env.S3_BUCKET,
          Key: process.env.S3_PATH+file,
          Body: tempCrs
        };
        try {

          let uploadPromise = await s3.upload(params).promise();
          console.log("Successfully uploaded file: "+file);

        } catch (e) {
          console.log("Error uploading data: ", e);
        }
      }
      tempCrs.on('close', () => {
        if(fs.existsSync(tempFilename)) {

          fs.unlink(tempFilename, (err) => {
            if(err) {
              console.log("Could not delete split CSV file: "+tempFilename, err);
            }
            // if no error, file has been deleted successfully
            console.log("Deleted split CSV file: "+tempFilename);
          });

        } else {
          console.log("Split CSV file not created yet: "+tempFilename);
        }
      });

    });

    resolve(true);

  });
};

/**
 * To extract csv files from zip file and split those into small files
 */
module.exports.splitFiles = async event => {

  const unzipper = require('unzipper');
  const exec = require('child_process').exec;

  return new Promise( async (resolve, reject) => {
    const testFolder = process.env.LOCAL_TEMP_PATH;
    await fs.readdirSync(testFolder).forEach(file => {
      let tempFilename = testFolder+file;
      fs.createReadStream(tempFilename)
      .pipe(unzipper.Parse())
      .on('entry', async (entry) => {
        // store main csv file to local split path
        let tempCsvFilepath = process.env.LOCAL_SPLIT_PATH+entry.path;
        let tempCws = await entry.pipe(fs.createWriteStream(tempCsvFilepath));
        tempCws.on('finish', () => {

          // split main csv file into chunks
            let tempCommand = "split -d -l "+process.env.NR_SPLIT_FILES+" --additional-suffix="+process.env.SPLIT_FILE_EXTENSION+" "+tempCsvFilepath+" "+process.env.LOCAL_SPLIT_PATH+process.env.SPLIT_FILE_PREFIX;

            let child;
            child = exec(tempCommand, (error, stdout, stderr) => {

              if(error !== null) {
                console.log('error: ' + error);
                reject(error);
              }

            });

        });
        tempCws.on('close', () => {
          if(fs.existsSync(tempCsvFilepath)) {

            // remove main csv file after splitting done
            fs.unlink(tempCsvFilepath, (err) => {
              if(err) {
                console.log("Could not delete main CSV file: "+tempCsvFilepath, err);
              }

              // if no error, file has been deleted successfully
              console.log("Deleted main CSV file: "+tempCsvFilepath);

              // remove zip file
              if(fs.existsSync(tempFilename)) {

                fs.unlink(tempFilename, (err) => {
                  if(err) {
                    console.log("Could not delete main ZIP file: "+tempFilename, err);
                  }
                  // if no error, file has been deleted successfully
                  console.log("Deleted ZIP file: "+tempFilename);

                });

              } else {
                console.log("ZIP file not created yet: "+tempFilename);
              }

            });

          } else {
            console.log("Main CSV file not created yet: "+tempCsvFilepath);
          }

        });

      });
    });
    resolve(true);
  });
};
